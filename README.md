Founders Florence and Aurélie are now travelling the globe in search of products that elevate the everyday and bring joy through elegant design­­. They have collaborated with young designers and artists to curate their online concept store.

Address: 1385 5th Street, Sarasota, FL 34236, USA

Phone: 941-312-1081

Website: https://moiconceptstore.com
